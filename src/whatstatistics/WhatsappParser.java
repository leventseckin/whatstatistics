package whatstatistics;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class WhatsappParser {
    public static ArrayList<WhatsappMessage> uglyParser(String filename){
        String line = null;
        String date = null, name = null, message = "";
        boolean validMessage = false;
        ArrayList<WhatsappMessage> messages = new ArrayList<WhatsappMessage>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));

            while ((line = br.readLine()) != null) {

                String first = line.split(", ")[0];
                if(first.length() == 10 && first.charAt(2) == '/' && first.charAt(5) == '/'){//
                    porcessLastMessage(messages, date,name,message, validMessage);

                    int beginIndex = line.indexOf('-') + 2;
                    int endIndex = line.indexOf(':', beginIndex);
                    if(endIndex < 0){
                        //Special Line

                        message = line;
                        validMessage = false;
                        continue;
                    }

                    date = line.substring(0,beginIndex-3);
                    name = line.substring(beginIndex, endIndex);
                    message = line.substring(endIndex+2);
                    validMessage = true;
                }
                else{
                    message += " "+line;
                }
            }
            porcessLastMessage(messages, date, name, message, validMessage);


        } catch (Exception e) {
            System.out.println(line);
            e.printStackTrace();
        }

        return messages;
    }

    private static void porcessLastMessage(ArrayList<WhatsappMessage> messages, String date, String name, String message, boolean validMessage) {
        if(validMessage)
            messages.add(new WhatsappMessage(name, date, message));
        else
            System.out.println("Not valid message: "+message);
    }

    public static HashMap<String, WhatsappUser> conversationParser(ArrayList<WhatsappMessage> messages) {
        HashMap<String, WhatsappUser> users = new HashMap<>();
        Date lastTime = null;
        WhatsappUser lastUser = null;
        boolean ignored = true;
        int ignoreCount = 0;
        for (int i = 0; i < messages.size(); i++) {
            WhatsappMessage m = messages.get(i);
            String name = m.getName();
            WhatsappUser user = users.get(name);
            if(user == null) users.put(name, user = new WhatsappUser(name));
            user.addMessage(m);

            ignoreCount += m.getMessage().length();
            if(lastTime != null && m.getDate().getTime() - lastTime.getTime() > WhatStatistics.CONVERSATION_STOP){
                m.setConversationStart();
                messages.get(i-1).setConversationEnd();
                if(ignored) messages.get(i-1).setIgnored(ignoreCount);
                ignored = true;
                ignoreCount = 0;
                lastUser = user;
            }
            else{
                if(lastUser == null) lastUser = user;
                if(lastUser != user) ignored = false;
            }
            lastTime =  m.getDate();
        }
        return users;
    }

    public static String rightPad(String str, int length, char charToFill) {
        while (str.length() < length)
            str += charToFill;
        return str;
    }
}
