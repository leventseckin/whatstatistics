package whatstatistics;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * Created by Levent on 14.12.2015.
 */
public class WhatStatistics {
    public static final long CONVERSATION_STOP = TimeUnit.HOURS.toMillis(3);

    private ArrayList<WhatsappMessage> messages;
    private HashMap<String,WhatsappUser> users ;
    private int totalChar;
    private int totalMessage;
    private int totalMedia;
    private int i;

    public WhatStatistics(String filename) {
        messages = WhatsappParser.uglyParser(filename);
        users = WhatsappParser.conversationParser(messages);
        analyse();
    }

    private void analyse() {
        totalChar = 0;
        totalMessage = 0;
        totalMedia = 0;
        users.values().forEach(u ->  u.analyse(this));
        users.values().forEach(u ->  u.analyseRatio(this));
        //users.values().forEach(u -> System.out.println(u) );

        ArrayList<WhatsappUser> userList = new ArrayList<>(users.values());

        report("Yollanan char sayısı", userList, WhatsappUser::getCharCount, WhatsappUser::getCharCountReport);
        report("Mesaj atma sayısı", userList, WhatsappUser::getMessageCount, WhatsappUser::getMessageCountReport);
        report("Mesaj başına char oranı", userList, WhatsappUser::getMessageCharRatio, WhatsappUser::getCharRatioReport);
        report("Media sayısı", userList, WhatsappUser::getMediaCount, WhatsappUser::getMediaCountReport);
        report("En uzun tek parça mesaj char sayısı", userList, WhatsappUser::getLongestMessageLength, WhatsappUser::LongestMessageReport);
        report("En az 3 saat sessiz gruba ilk mesaj yazma (konuşma başlatma)", userList, WhatsappUser::getConversationStartCount, WhatsappUser::ConversationStartReport);
        report("En az 3 saat sessizlik oluşturan mesajı yollama (konuşma bitirme)", userList, WhatsappUser::getConversationEndCount, WhatsappUser::ConversationEndReport);
        report("En az 3 saat cevap alınmayan mesaj atma (başarısız konuşma başlatma(sallanmama))", userList, WhatsappUser::getConversationIgnoredCount, WhatsappUser::ConversationIgnoredReport);
        report("En uzun sallanılmayan mesaj dizisi char sayısı", userList, WhatsappUser::getLongestIgnoredConversation, WhatsappUser::LongestIgnoredConversationReport);
    }

    private void report(String title, ArrayList<WhatsappUser> userList, Function<WhatsappUser, WSNumber> sorter, Function<WhatsappUser, String> report){
        i=0;
        System.out.println();
        System.out.println(title + ":");
        sort(userList, sorter);
        userList.forEach(u -> System.out.println(String.format("%2d", (++i)) + ") " +u.getFormattedName() + sorter.apply(u).getValue() + "\t " + report.apply(u)));
    }

    private <T>void sort(ArrayList<T> userList, Function<T, WSNumber> f) {
        Collections.sort(userList, ( T o1, T o2) ->
                f.apply(o2).compareTo(f.apply(o1)));
    }

    public void addCharCount(int charCount) {
        totalChar += charCount;
    }

    public void addMessageCount(int messageCount) {
        totalMessage += messageCount;
    }

    public void addMediaCount(int mediaCount) {
        totalMedia += mediaCount;
    }

    public int getTotalChar() {
        return totalChar;
    }

    public int getTotalMessage() {
        return totalMessage;
    }

    public int getTotalMedia() {
        return totalMedia;
    }

    /*
    *
    * */
    public static void main(String[] args) {
        new WhatStatistics(args[0]);
    }
}
