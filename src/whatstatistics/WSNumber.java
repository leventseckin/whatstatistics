package whatstatistics;

/**
 * Created by Levent on 15.12.2015.
 */
public class WSNumber<T extends Number> {
    private WSNumberOperable wrapper;

    public WSNumber(T number){
        if(number instanceof Integer)
            wrapper = new WSIntegerWrapper(number);
        else if(number instanceof Float)
            wrapper = new WSFloatWrapper(number);
        else
            wrapper = new WSIntegerWrapper(number);;
    }

    public int compareTo(WSNumber other){
        return wrapper.compareTo(other.getValue());
    }

    public void inc() {
        wrapper.inc();
    }

    public void add(T add) {
        wrapper.add(add);
    }

    public String toString() {
        return wrapper.toString();
    }

    public void set(T number) {
        wrapper.set(number);
    }

    @SuppressWarnings("unchecked")
    public T getValue() {
        return (T) wrapper.getValue();
    }

    interface WSNumberOperable {
        void inc();
        void add(Number t);
        int compareTo(Number other);
        Number getValue();
        void set(Number number);
    }

    class WSIntegerWrapper implements WSNumberOperable {
        Integer integer;

        public WSIntegerWrapper(T number) {
            set(number);
        }

        @Override
        public void inc() {
            integer++;
        }

        @Override
        public void add(Number t) {
            integer += (Integer)t;
        }

        @Override
        public int compareTo(Number other) {
            return integer.compareTo((Integer)other);
        }

        @Override
        public Number getValue() {
            return integer;
        }

        @Override
        public void set(Number number) {
            integer = (Integer)number;
        }

        @Override
        public String toString() {
            return ""+integer;
        }
    }

    class WSFloatWrapper implements WSNumberOperable {
        Float aFloat;

        public WSFloatWrapper(T number) {
            set(number);
        }

        @Override
        public void inc() {
            aFloat++;
        }

        @Override
        public void add(Number t) {
            aFloat += (Float)t;
        }

        @Override
        public int compareTo(Number other) {
            return aFloat.compareTo((Float)other);
        }

        @Override
        public Number getValue() {
            return aFloat;
        }

        @Override
        public void set(Number number) {
            aFloat = (Float)number;
        }

        @Override
        public String toString() {
            return String.format("%.2f", aFloat);
        }
    }
}
