package whatstatistics;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Levent on 14.12.2015.
 */
public class WhatsappMessage {
    static DateFormat format = new SimpleDateFormat("dd/MM/yyyy, HH:mm", Locale.ENGLISH);

    private String name;
    private Date date;
    private String message;
    private boolean media = false;
    private boolean start = false;
    private boolean end = false;
    private int ignored = 0;

    public WhatsappMessage(String date, String message) {
        try {
            this.date = format.parse(date);
        } catch (ParseException e) {}

        if(message.equals("<Media omitted>")){
            this.message = "";
            this.media = true;
        }
        else{
            this.message = message;

        }
    }

    public WhatsappMessage(String name, String date, String message) {
        this(date,message);
        this.name = name;
    }

    public void setConversationStart() {
        start = true;
    }

    public void setConversationEnd() {
        end = true;
    }

    public void setIgnored(int ignoreCount) {
        ignored = ignoreCount;
    }

    public String getName() { return name; }

    public Date getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    public boolean isMedia() {
        return media;
    }

    public boolean isStart() {
        return start;
    }

    public boolean isEnd() {
        return end;
    }

    public int getIgnored() {
        return ignored;
    }

    public String summary(int charCount) {
        return format.format(date) + " " + message.substring(0, message.length()>charCount?charCount:message.length());
    }
}
