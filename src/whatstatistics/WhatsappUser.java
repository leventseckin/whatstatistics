package whatstatistics;

import java.util.ArrayList;

/**
 * Created by Levent on 14.12.2015.
 */
public class WhatsappUser {
    private final String name;
    private final ArrayList<WhatsappMessage> messages;

    private WSNumber<Integer> messageCount;
    private WSNumber<Integer> mediaCount;
    private WSNumber<Integer> charCount;
    private WSNumber<Float> messageCharRatio;
    private WSNumber<Integer> longestMessageLength;
    private WhatsappMessage longestMessage;
    private WSNumber<Integer> conversationStartCount;
    private WSNumber<Integer> conversationEndCount;
    private WSNumber<Integer> conversationIgnoredCount;
    private WSNumber<Integer> longestIgnoredConversation;
    private WhatsappMessage longestIgnoredMessage;
    private WSNumber<Float> charRatio;
    private WSNumber<Float> messageRatio;
    private WSNumber<Float> mediaRatio;

    public WhatsappUser(String name) {
        this.name = name;
        this.messages = new ArrayList<>();
    }

    public void addMessage(String date, String message) {
        messages.add(new WhatsappMessage(date,message));
    }

    public void addMessage(WhatsappMessage message) {
        messages.add(message);
    }

    public void analyse(WhatStatistics whatStatistics) {
        messageCount = new WSNumber<Integer>(0);
        mediaCount = new WSNumber<Integer>(0);
        charCount = new WSNumber<Integer>(0);
        longestMessageLength = new WSNumber<Integer>(0);
        longestMessage = null;
        conversationStartCount = new WSNumber<Integer>(0);
        conversationEndCount = new WSNumber<Integer>(0);
        conversationIgnoredCount = new WSNumber<Integer>(0);
        longestIgnoredConversation = new WSNumber<Integer>(0);

        for (WhatsappMessage message : messages) {
            if(message.isMedia()){
                mediaCount.inc();
            }
            else{
                messageCount.inc();
                int length = message.getMessage().length();
                charCount.add(length);
                if(length > longestMessageLength.getValue()){
                    longestMessage = message;
                    longestMessageLength.set(length);
                }

                if(message.isStart())
                    conversationStartCount.inc();

                if(message.isEnd())
                    conversationEndCount.inc();

                if(message.getIgnored() > 0)
                    conversationIgnoredCount.inc();

                if(message.getIgnored() > longestIgnoredConversation.getValue()){
                    longestIgnoredMessage = message;
                    longestIgnoredConversation.set(message.getIgnored());
                }
            }
        }
        whatStatistics.addCharCount(charCount.getValue());
        whatStatistics.addMessageCount(messageCount.getValue());
        whatStatistics.addMediaCount(mediaCount.getValue());
    }

    public void analyseRatio(WhatStatistics whatStatistics) {
        messageCharRatio = new WSNumber<Float>((float)charCount.getValue() / (float)messageCount.getValue());
        charRatio = new WSNumber<Float>(100*(float)charCount.getValue() / (float)whatStatistics.getTotalChar());
        messageRatio = new WSNumber<Float>(100*(float)messageCount.getValue() / (float)whatStatistics.getTotalMessage());
        mediaRatio = new WSNumber<Float>(100*(float)mediaCount.getValue() / (float)whatStatistics.getTotalMedia());
    }

    public String getName() {
        return name;
    }

    public WSNumber<Integer> getMessageCount() {
        return messageCount;
    }

    public WSNumber<Integer> getMediaCount() {
        return mediaCount;
    }

    public WSNumber<Integer> getCharCount() {
        return charCount;
    }

    public WSNumber<Float> getMessageCharRatio() {
        return messageCharRatio;
    }

    public WSNumber<Integer> getLongestMessageLength() {
        return longestMessageLength;
    }

    public WhatsappMessage getLongestMessage() {
        return longestMessage;
    }

    public WSNumber<Integer> getConversationStartCount() {
        return conversationStartCount;
    }

    public WSNumber<Integer> getConversationEndCount() {
        return conversationEndCount;
    }

    public WSNumber<Integer> getConversationIgnoredCount() {
        return conversationIgnoredCount;
    }

    public WSNumber<Integer> getLongestIgnoredConversation() {
        return longestIgnoredConversation;
    }

    public WhatsappMessage getLongestIgnoredMessage() {
        return longestIgnoredMessage;
    }

    public WSNumber<Float> getCharRatio() {
        return charRatio;
    }

    public WSNumber<Float> getMessageRatio() {
        return messageRatio;
    }

    public WSNumber<Float> getMediaRatio() {
        return mediaRatio;
    }


    public String getFormattedName() {
        return WhatsappParser.rightPad(name, 18, ' ');
    }

    @Override
    public String toString() {
        String info = "";

        info += "Name: " +getName()+System.lineSeparator();
        info += "MessageCount: " +getMessageCount()+System.lineSeparator();
        info += "MessageRatio: %" +getMessageRatio()+System.lineSeparator();
        info += "CharCount: " +getCharCount()+System.lineSeparator();
        info += "CharRatio: %" +getCharRatio()+System.lineSeparator();
        info += "MediaCount: " +getMediaCount()+System.lineSeparator();
        info += "MediaRatio: %" +getMediaRatio()+System.lineSeparator();
        info += "MessageCharRatio: " + getMessageCharRatio()+System.lineSeparator();
        info += "LongestMessageLength: " + getLongestMessageLength()+System.lineSeparator();
        info += "LongestMessage: " + getLongestMessage().getMessage().substring(0,30)+System.lineSeparator();
        info += "ConversationStartCount: " + getConversationStartCount()+System.lineSeparator();
        info += "ConversationEndCount: " + getConversationEndCount()+System.lineSeparator();
        info += "ConversationIgnoredCount: " + getConversationIgnoredCount()+System.lineSeparator();
        info += "LongestIgnoredConversation: " + getLongestIgnoredConversation()+System.lineSeparator();
        if(longestIgnoredMessage != null)
            info += "LongestIgnoredMessage: " + getLongestIgnoredMessage().getMessage().substring(0,getLongestIgnoredMessage().getMessage().length()<30?getLongestIgnoredMessage().getMessage().length():30)+System.lineSeparator();

        return info;
    }

    public String getMessageCountReport() {
        return "%"+ messageRatio;
    }

    public String getCharCountReport() {
        return "%"+ charRatio;
    }

    public String getCharRatioReport() {
        return "";
    }

    public String getMediaCountReport() {
        return " %"+ mediaRatio;
    }

    public String LongestMessageReport() {
        return longestMessage.summary(20);
    }

    public String ConversationStartReport() {
        return "";
    }

    public String ConversationEndReport() {
        return "";
    }

    public String ConversationIgnoredReport() {
        return "";
    }

    public String LongestIgnoredConversationReport() {
        return longestIgnoredMessage!=null?longestIgnoredMessage.summary(20):"";
    }
}
